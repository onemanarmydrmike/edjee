package mm.lab.edjee.runners;

import java.util.List;
import mm.lab.edjee.dao.FileReader;
import mm.lab.edjee.model.Debtor;
import mm.lab.edjee.model.DecisionTree;
import org.apache.log4j.Logger;

public class Classification implements Exploration {

    private static final Logger LOGGER = Logger.getLogger(Classification.class);

    private Debtor testDebtor1 = new Debtor("Zenek", "Zenkowski", "MEZCZYZNA", "NISKIE", "PODSTAWOWE", "BEZROBOTNY", "powyzej_60");
    private Debtor testDebtor2 = new Debtor("Zenobia", "Zenkowska", "KOBIETA", "WYSOKIE", "WYZSZE", "UMOWA_NA_CZAS_NIEOKRESLONY", "od_30_do_40");

    @Override
    public void run() {
        List<Debtor> debtors = FileReader.readClassificationFile();

        DecisionTree decisionTree = new DecisionTree(debtors);
        decisionTree.train();

        LOGGER.info("Classification results: ");
        LOGGER.info(testDebtor1.toString() + "   ---> " + decisionTree.decide(testDebtor1));
        LOGGER.info(testDebtor2.toString() + "   ---> " + decisionTree.decide(testDebtor2));
    }
}
