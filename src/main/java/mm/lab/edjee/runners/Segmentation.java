package mm.lab.edjee.runners;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import mm.lab.edjee.dao.FileReader;
import mm.lab.edjee.model.Cluster;
import mm.lab.edjee.model.Noise;
import mm.lab.edjee.model.Point;
import org.apache.log4j.Logger;

public class Segmentation implements Exploration {

    private static final Logger LOGGER = Logger.getLogger(Segmentation.class);

    private static final Cluster NOISE = new Noise();
    private static List<Cluster> clusters = new ArrayList<>();

    @Override
    public void run() {
        List<Point> points = FileReader.readSegmentationFile();

        dbscan(points, 1.0, 4);
        points.forEach(point -> point.getCluster().addPoint(point));
        clusters.removeIf(cluster -> cluster.getPoints().isEmpty());

        LOGGER.info("Segmentation results: ");
        LOGGER.info("Points splitted into " + clusters.size() + " groups:");
        LOGGER.info(clusters.toString());
    }

    private void dbscan(List<Point> points, double eps, int minPts) {
        Cluster currentCluster = getNewCluster();
        Optional<Point> unclassifiedPoint = points.stream().filter(point -> point.getCluster() == null).findAny();
        while (unclassifiedPoint.isPresent()) {
            if (expandCluster(points, unclassifiedPoint.get(), currentCluster, eps, minPts)) {
                currentCluster = getNewCluster();
            }
            unclassifiedPoint = points.stream().filter(point -> point.getCluster() == null).findAny();
        }
    }

    private boolean expandCluster(List<Point> points, Point rootPoint, Cluster currentCluster, double eps, int minPts) {
        List<Point> seeds = getNeighbors(points, rootPoint, eps);

        if (seeds.size() < minPts) {
            rootPoint.setCluster(NOISE);
            return false;
        } else {
            rootPoint.setCluster(currentCluster);
            seeds.forEach(seed -> seed.setCluster(currentCluster));
            while (!seeds.isEmpty()) {
                Point seed = seeds.get(0);
                List<Point> neighbors = getNeighbors(points, seed, eps);
                if (neighbors.size() >= minPts) {
                    neighbors.forEach(neighbor -> {
                        Cluster neighborCluster = neighbor.getCluster();
                        if (neighborCluster == null) {
                            seeds.add(neighbor);
                            neighbor.setCluster(currentCluster);
                        } else if (neighborCluster.equals(NOISE)) {
                            neighbor.setCluster(currentCluster);
                        }
                    });
                }
                seeds.remove(seed);
            }
            return true;
        }
    }

    private List<Point> getNeighbors(List<Point> points, Point rootPoint, double eps) {
        return points.stream().filter(point -> dist(rootPoint, point) <= eps).collect(Collectors.toList());
    }

    private Cluster getNewCluster() {
        Cluster newCluster = new Cluster("Cluster " + (clusters.size() + 1));
        clusters.add(newCluster);
        return newCluster;
    }

    private double dist(Point a, Point b) {
        double sum = 0.0;
        List<Double> coordinatesA = a.getCoordinates();
        List<Double> coordinatesb = b.getCoordinates();
        for (int i = 0; i < coordinatesA.size(); i++) {
            sum += Math.pow(coordinatesb.get(i) - coordinatesA.get(i), 2);
        }
        return Math.sqrt(sum);
    }
}
