package mm.lab.edjee.runners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import mm.lab.edjee.dao.FileReader;
import mm.lab.edjee.model.Transaction;
import org.apache.log4j.Logger;

public class Association implements Exploration {

    private static final Logger LOGGER = Logger.getLogger(Association.class);

    private static final Double SUPPORT = 0.04;

    @Override
    public void run() {
        List<Transaction> transactions = FileReader.readAssociationFile();
        List<List<String>> firstCandidates = getFirstCandidates(transactions);

        List<List<String>> results = apriori(transactions, firstCandidates);
        LOGGER.info("Association results: ");
        results.forEach(result -> LOGGER.info(String.join(", ", result)));
    }

    private List<List<String>> apriori(List<Transaction> transactions, List<List<String>> candidates) {
        List<List<String>> supportedCandidates = new ArrayList<>();
        candidates.forEach(candidate -> {
            Double counter = 0.0;
            for (Transaction transaction : transactions) {
                List<String> productsFromTheTransaction = transaction.getProducts();
                if (productsFromTheTransaction.containsAll(candidate)) {
                    counter++;
                }
            }
            if (counter / transactions.size() >= SUPPORT) {
                supportedCandidates.add(candidate);
            }
        });
        if (supportedCandidates.isEmpty()) {
            return candidates;
        }
        List<List<String>> newCandidates = getNewCandidates(supportedCandidates);
        return apriori(transactions, newCandidates);
    }

    private List<List<String>> getFirstCandidates(List<Transaction> transactions) {
        Set<String> products = new HashSet<>();
        transactions.forEach(transaction -> products.addAll(transaction.getProducts()));

        List<List<String>> candidates = new ArrayList<>();
        products.forEach(product -> candidates.add(Collections.singletonList(product)));

        return candidates;
    }

    private List<List<String>> getNewCandidates(List<List<String>> supportedCandidates) {
        Set<String> allSupportedProducts = new HashSet<>();
        supportedCandidates.forEach(allSupportedProducts::addAll);

        return findAllCombinations(allSupportedProducts, supportedCandidates.get(0).size() + 1);
    }

    private List<List<String>> findAllCombinations(Set<String> elementsToCombineFrom, int numberOfElementsInCombination) {
        String[] arr = elementsToCombineFrom.toArray(new String[0]);

        // A temporary array to store all combination one by one
        String[] tempCombination = new String[numberOfElementsInCombination];

        return combine(arr, tempCombination, 0, elementsToCombineFrom.size() - 1, 0, numberOfElementsInCombination);
    }

    private List<List<String>> combine(String[] arr, String[] tempCombination, int start, int end, int index, int numberOfElementsInCombination) {
        List<List<String>> result = new ArrayList<>();

        // Current combination is ready to be stored
        if (index == numberOfElementsInCombination) {
            List<String> combination = new ArrayList<>(Arrays.asList(tempCombination).subList(0, numberOfElementsInCombination));
            result.add(combination);
            return result;
        }

        /*
         * replace index with all possible elements.
         * The condition "end-i+1 >= numberOfElementsInCombination-index" makes sure that including one element at index will make a combination with remaining elements at remaining positions
         */
        for (int i = start; i <= end && end - i + 1 >= numberOfElementsInCombination - index; i++) {
            tempCombination[index] = arr[i];
            result.addAll(combine(arr, tempCombination, i + 1, end, index + 1, numberOfElementsInCombination));
        }
        return result;
    }
}
