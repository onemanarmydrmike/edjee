package mm.lab.edjee.model;

import java.util.ArrayList;
import java.util.List;

public class Cluster {

    protected String name;
    protected List<Point> points;

    public Cluster(String name) {
        this.name = name;
        this.points = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void addPoint(Point point) {
        this.points.add(point);
    }

    @Override
    public String toString() {
        return "\n\nCluster name='" + name
            + "'\nPoints=" + points;
    }
}
