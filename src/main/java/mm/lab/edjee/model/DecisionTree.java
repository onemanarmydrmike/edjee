package mm.lab.edjee.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DecisionTree {

    private List<Debtor> trainingDebtors;
    private Node rootNode;

    public DecisionTree(List<Debtor> debtorsFromFile) {
        this.trainingDebtors = debtorsFromFile;
    }

    public void train() {
        this.rootNode = createNode(trainingDebtors, Arrays.asList(Attribute.values()));
    }

    private Node createNode(List<Debtor> trainingData, List<Attribute> attributes) {
        Attribute bestAttribute = id3(trainingData, attributes);

        Map<String, Node> branches = new HashMap<>();
        for (String propName : bestAttribute.getProperties()) {
            List<Debtor> subTreeTrainingData = trainingData.stream().filter(debtor -> debtor.getAttributeValue(bestAttribute).equals(propName)).collect(Collectors.toList());
            List<Attribute> subTreeAttributes = attributes.stream().filter(attribute -> attribute != bestAttribute).collect(Collectors.toList());
            if (subTreeAttributes.isEmpty()) {
                boolean isSolvent = checkIfSolvent(subTreeTrainingData);
                branches.put(propName, new Node(isSolvent));
            } else {
                branches.put(propName, createNode(subTreeTrainingData, subTreeAttributes));
            }
        }
        return new Node(bestAttribute, branches);
    }

    private boolean checkIfSolvent(List<Debtor> subTreeTrainingData) {
        int numberOfSolventDebtors = (int) subTreeTrainingData.stream().filter(Debtor::isSolvent).count();
        double percentageOfSolventDebtors = (double) numberOfSolventDebtors / subTreeTrainingData.size();
        return percentageOfSolventDebtors > 0.5;
    }

    private Attribute id3(List<Debtor> trainingDebtors, List<Attribute> attributes) {
        Attribute bestAttribute = attributes.get(0);

        double bestAttributeEntropy = 1.0;
        for (Attribute attribute : attributes) {
            double currentAttributeEntropy = calculateEntropy(trainingDebtors, attribute);
            if (currentAttributeEntropy < bestAttributeEntropy) {
                bestAttributeEntropy = currentAttributeEntropy;
                bestAttribute = attribute;
            }
        }
        return bestAttribute;
    }

    private Double calculateEntropy(List<Debtor> trainingData, Attribute attribute) {
        Double result = 0.0;

        for (String propName : attribute.getProperties()) {
            int propertySize = (int) trainingData.stream().filter(debtor -> debtor.getAttributeValue(attribute).equals(propName)).count();
            int numberOfSolventDebtors = (int) trainingData.stream().filter(debtor -> debtor.isSolvent() && debtor.getAttributeValue(attribute).equals(propName)).count();
            int numberOfNonSolventDebtors = (propertySize - numberOfSolventDebtors);

            double percentageOfSolventDebtors = (double) numberOfSolventDebtors / propertySize;
            double percentageOfNonSolventDebtors = (double) numberOfNonSolventDebtors / propertySize;
            double entropy = -((percentageOfSolventDebtors * Math.log(percentageOfSolventDebtors)) + (percentageOfNonSolventDebtors * Math.log(percentageOfNonSolventDebtors)));

            result += ((double) propertySize / trainingData.size()) * entropy;
        }
        return result;
    }

    public boolean decide(Debtor testDebtor) {
        Node currentNode = rootNode;
        while (currentNode.getBranches() != null) {
            currentNode = currentNode.getBranches().get(testDebtor.getAttributeValue(currentNode.getAttribute()));
        }
        return currentNode.isSolvent();
    }
}
