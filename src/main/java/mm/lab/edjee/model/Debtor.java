package mm.lab.edjee.model;

import java.lang.reflect.Field;
import org.apache.log4j.Logger;

public class Debtor {

    private static final Logger LOGGER = Logger.getLogger(Debtor.class);

    private String firstName;
    private String lastName;
    private String gender;
    private String earnings;
    private String education;
    private String employment;
    private String age;
    private boolean solvent;

    public Debtor(String firstName, String lastName, String gender, String earnings, String education, String employment, String age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.earnings = earnings;
        this.education = education;
        this.employment = employment;
        this.age = age;
    }

    public Debtor(String[] dataFromFile) {
        this.firstName = dataFromFile[0];
        this.lastName = dataFromFile[1];
        this.gender = dataFromFile[2];
        this.earnings = dataFromFile[3];
        this.education = dataFromFile[4];
        this.employment = dataFromFile[5];
        this.age = dataFromFile[6];
        this.solvent = Boolean.valueOf(dataFromFile[7]);
    }

    public String getAttributeValue(Attribute attribute) {
        try {
            Field field = this.getClass().getDeclaredField(attribute.getFieldName());
            field.setAccessible(true);
            return (String) field.get(this);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public boolean isSolvent() {
        return solvent;
    }

    @Override
    public String toString() {
        return "Debtor{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", gender='" + gender + '\'' +
            ", earnings='" + earnings + '\'' +
            ", education='" + education + '\'' +
            ", employment='" + employment + '\'' +
            ", age='" + age + '\'' +
            '}';
    }
}
