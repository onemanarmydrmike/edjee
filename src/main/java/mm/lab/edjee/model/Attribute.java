package mm.lab.edjee.model;

import java.util.Arrays;
import java.util.List;

public enum Attribute {

    GENDER("gender", Arrays.asList("MEZCZYZNA", "KOBIETA")),
    EARNINGS("earnings", Arrays.asList("NISKIE", "SREDNIE", "WYSOKIE")),
    EDUCATION("education", Arrays.asList("PODSTAWOWE", "SREDNIE", "WYZSZE")),
    EMPLOYMENT("employment", Arrays.asList("BEZROBOTNY", "UMOWA_O_DZIELO", "UMOWA_NA_CZAS_NIEOKRESLONY", "SAMOZATRUDNIENIE")),
    AGE("age", Arrays.asList("od_20_do_30", "od_30_do_40", "od_40_do_50", "od_50_do_60", "powyzej_60"));

    private String fieldName;
    private List<String> properties;

    Attribute(String fieldName, List<String> properties) {
        this.fieldName = fieldName;
        this.properties = properties;
    }

    public String getFieldName() {
        return fieldName;
    }

    public List<String> getProperties() {
        return properties;
    }
}
