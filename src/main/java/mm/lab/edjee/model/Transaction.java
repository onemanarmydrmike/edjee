package mm.lab.edjee.model;

import java.util.ArrayList;
import java.util.List;

public class Transaction {

    private List<String> products;

    public Transaction() {
        this.products = new ArrayList<>();
    }

    public Transaction addProduct(String product) {
        this.products.add(product);
        return this;
    }

    public List<String> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "products=" + products +
            '}';
    }
}
