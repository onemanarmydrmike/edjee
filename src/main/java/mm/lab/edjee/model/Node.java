package mm.lab.edjee.model;

import java.util.Map;

public class Node {

    private Attribute attribute;
    private Map<String, Node> branches;
    private boolean solvent;

    public Node(Attribute attribute, Map<String, Node> branches) {
        this.attribute = attribute;
        this.branches = branches;
    }

    public Node(boolean solvent) {
        this.solvent = solvent;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public Map<String, Node> getBranches() {
        return branches;
    }

    public boolean isSolvent() {
        return solvent;
    }

    @Override
    public String toString() {
        return "Node{" +
            "attribute=" + attribute +
            ", branches=" + branches +
            ", solvent=" + solvent +
            '}';
    }
}
