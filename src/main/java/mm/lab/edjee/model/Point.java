package mm.lab.edjee.model;

import java.util.List;

public class Point {

    private List<Double> coordinates;
    private Cluster cluster;

    public Point(List<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public String toString() {
        return "\nPoint{coordinates=" + coordinates;
    }
}
