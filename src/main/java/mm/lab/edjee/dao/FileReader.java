package mm.lab.edjee.dao;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import mm.lab.edjee.model.Debtor;
import mm.lab.edjee.model.Point;
import mm.lab.edjee.model.Transaction;
import org.apache.log4j.Logger;

public class FileReader {

    private static final Logger LOGGER = Logger.getLogger(FileReader.class);

    private FileReader() {
    }

    public static List<Transaction> readAssociationFile() {
        List<Transaction> transactions = new ArrayList<>();

        try {
            List<String> rows = Files.readAllLines(Paths.get(FileReader.class.getClassLoader().getResource("files/asocjacja.txt").toURI()));
            rows.forEach(row -> {
                Transaction transaction = new Transaction();
                String substring = row.substring(row.indexOf('[') + 1, row.lastIndexOf(']'));
                String[] products = substring.split(", ");
                Arrays.asList(products).forEach(transaction::addProduct);
                transactions.add(transaction);
            });
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return transactions;
    }

    public static List<Debtor> readClassificationFile() {
        List<Debtor> debtors = new ArrayList<>();

        try {
            List<String> rows = Files.readAllLines(Paths.get(FileReader.class.getClassLoader().getResource("files/klasyfikacja.txt").toURI()));
            rows.forEach(row -> debtors.add(new Debtor(row.split(" "))));
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return debtors;
    }

    public static List<Point> readSegmentationFile() {
        List<Point> points = new ArrayList<>();

        try {
            List<String> rows = Files.readAllLines(Paths.get(FileReader.class.getClassLoader().getResource("files/segmentacja.txt").toURI()));
            rows.forEach(row -> {
                String[] coordinates = row.substring(1, row.length() - 1).split(", ");
                Point point = new Point(Stream.of(coordinates).map(Double::valueOf).collect(Collectors.toList()));
                points.add(point);

            });
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return points;
    }
}
