package mm.lab.edjee;

import mm.lab.edjee.runners.Association;
import mm.lab.edjee.runners.Classification;
import mm.lab.edjee.runners.Exploration;
import mm.lab.edjee.runners.Segmentation;
import org.apache.log4j.Logger;

public class EDJEERunner {

    private static final Logger LOGGER = Logger.getLogger(EDJEERunner.class);

    private static final String OPTIONS = "'a' for association, 'c' for classification";

    public static void main(String[] args) {
        LOGGER.info("Eksploracja danych na platformie Java EE");

        if (args.length == 0) {
            LOGGER.info("Choose exploration to run. " + OPTIONS);
        } else {
            String arg = args[0];
            Exploration exploration;
            if (arg.equalsIgnoreCase("a")) {
                exploration = new Association();
            } else if (arg.equalsIgnoreCase("c")) {
                exploration = new Classification();
            } else if (arg.equalsIgnoreCase("s")) {
                exploration = new Segmentation();
            } else {
                LOGGER.info("Wrong argument. Available options: " + OPTIONS);
                return;
            }
            exploration.run();
        }
    }
}
